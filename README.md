infra.apache
=========

A role to install and configure httpd on a server

Requirements
------------

None.

Role Variables
--------------

Copy the main.yml file in the defaults directory to the appropriate group_vars directory

Dependencies
------------

None

Example Playbook
----------------

Below is an example of how to use the role

    - hosts: web_servers
      roles:
         - role: infra.apache

License
-------

BSD
